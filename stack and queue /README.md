### BaseballGame.java
You are keeping score for a baseball game with strange rules. The game consists of several rounds, where the scores of past rounds may affect future rounds' scores.

At the beginning of the game, you start with an empty record. You are given a list of strings ops, where `ops[i]` is the ith operation you must apply to the record and is one of the following:

An integer x - Record a new score of x.  
"+" - Record a new score that is the sum of the previous two scores. It is guaranteed there will always be two previous scores.  
"D" - Record a new score that is double the previous score. It is guaranteed there will always be a previous score.  
"C" - Invalidate the previous score, removing it from the record. It is guaranteed there will always be a previous score.  
Return the sum of all the scores on the record.  

### MinStack.java
Design a stack that supports push, pop, top, and retrieving the minimum element in constant time.

Implement the MinStack class:

`MinStack()` initializes the stack object.  
`void push(int val)` pushes the element val onto the stack.  
`void pop()` removes the element on the top of the stack.  
`int top()` gets the top element of the stack.  
`int getMin()` retrieves the minimum element in the stack.  

### MyQueue.java 
Implement a first in first out (FIFO) queue using only two stacks.  
The implemented queue should support all the functions of a normal queue (push, peek, pop, and empty).

Implement the MyQueue class:

`void push(int x)` Pushes element x to the back of the queue.  
`int pop()` Removes the element from the front of the queue and returns it.  
`int peek()` Returns the element at the front of the queue.  
`boolean empty()` Returns true if the queue is empty, false otherwise.  

### MyStack.java 
Implement a last-in-first-out (LIFO) stack using only two queues. The implemented stack should support all the functions of a normal stack (push, top, pop, and empty).

Implement the `MyStack` class:

`void push(int x)` Pushes element x to the top of the stack.  
`int pop()` Removes the element on the top of the stack and returns it.  
`int top()` Returns the element on the top of the stack.  
`boolean empty()` Returns true if the stack is empty, false otherwise.  

### ValidParantheses.java
Given a string s containing just the characters `'(', ')', '{', '}', '['` and `']'`, determine if the input string is valid.

An input string is valid if:

Open brackets must be closed by the same type of brackets.  
Open brackets must be closed in the correct order. 

### RadixSort.java
Radix sort of a queue.
