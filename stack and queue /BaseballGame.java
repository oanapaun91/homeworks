package queue_stack;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;

public class BaseballGame {
	public static int calPoints(String[] ops) {     
		Deque<Integer> myList = new ArrayDeque<>();
		for (int i = 0; i < ops.length; i++){ 

			if (ops[i].charAt(0) == '+') {    
				int previous = myList.pop().intValue();
				int beforePrevious = myList.pop().intValue();
				int newNumber = previous + beforePrevious;

				myList.push(beforePrevious);
				myList.push(previous);
				myList.push(newNumber);  

			}else if (ops[i].charAt(0) == 'D'){
				int newNumber = myList.peek().intValue() * 2;
				myList.push(newNumber);


			} else if (ops[i].charAt(0) == 'C'){
				myList.pop();

			} else {
				myList.push(Integer.valueOf(ops[i]));
			}
		}
		int sum = 0;

		while (!myList.isEmpty()){
			sum += myList.pop().intValue();    
		}

		return sum; 

	}

	public static void main(String[] args) {
		String[] ops = {"5","2","C","D","+"};
		System.out.print(calPoints(ops));
	}
}
