package queue_stack;

import java.util.LinkedList;
import java.util.Queue;
import java.util.ArrayList;

public class RadixSort {

	public static Queue<Integer> sort(Queue<Integer> ini, int counter) {	
		
		Queue<Integer> zero = new LinkedList<>();
		Queue<Integer> one = new LinkedList<>();
		Queue<Integer> two = new LinkedList<>();
		Queue<Integer> three = new LinkedList<>();
		Queue<Integer> four = new LinkedList<>();
		Queue<Integer> five = new LinkedList<>();
		Queue<Integer> six = new LinkedList<>();
		Queue<Integer> seven = new LinkedList<>();
		Queue<Integer> eight = new LinkedList<>();
		Queue<Integer> nine = new LinkedList<>();


		while (!ini.isEmpty()) {

			int x = ini.poll();
			int n = x/counter % 10;
			switch (n) {
			case 0:
				zero.offer(x);
				break;

			case 1:
				one.offer(x);
				break;

			case 2:
				two.offer(x);
				break;

			case 3:
				three.offer(x);
				break;

			case 4:
				four.offer(x);
				break;

			case 5:
				five.offer(x);
				break;

			case 6:
				six.offer(x);
				break;

			case 7:
				seven.offer(x);
				break;

			case 8:
				eight.offer(x);
				break;

			case 9:
				nine.offer(x);
				break;
			}
		}


		ArrayList<Queue<Integer>> listOfQueues = new ArrayList<>();
		listOfQueues.add(zero);
		listOfQueues.add(one);
		listOfQueues.add(two);
		listOfQueues.add(three);
		listOfQueues.add(four);
		listOfQueues.add(five);
		listOfQueues.add(six);
		listOfQueues.add(seven);
		listOfQueues.add(eight);
		listOfQueues.add(nine);

		for (int j = 0; j < listOfQueues.size(); j ++){
			while(!listOfQueues.get(j).isEmpty()) {
				int z = listOfQueues.get(j).poll();
				ini.offer(z);
			}
		}
		return ini;
	}

	public static void radixSort(Queue<Integer> ini) {
		int counter = 1;
		Queue<Integer> result = new LinkedList<>(); 
		int x = maximDigits(ini);
		
		for (int i = 0; i < x; i++) {
			result = sort(ini, counter);
			counter *= 10;
		}
		System.out.println(result);
	}

	public static int maximDigits(Queue<Integer> ini) {
		int max = Integer.MIN_VALUE;
		int queueSize = ini.size();
		Queue<Integer> aux = new LinkedList<>();
		
		for (int i = 0; i < queueSize;i++){
			int digits = 0;
			int x = ini.poll();
			aux.add(x);
			while (x != 0) {
				x = x/10;
				digits++;
			}
			if (digits >= max) {
				max = digits;
			}
		}
		ini.addAll(aux);
		return max;
	}

	public static void main(String[] args) {
		Queue<Integer> ini = new LinkedList<>();
		ini.add(10000);
		ini.add(4);
		ini.add(25);
		ini.add(319);
		ini.add(88);
		ini.add(51);
		ini.add(3430);
		ini.add(8471);
		ini.add(701);
		ini.add(1);
		ini.add(2989);
		ini.add(657);
		ini.add(713);

		radixSort(ini);

	}

}
