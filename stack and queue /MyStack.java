package queue_stack;

import java.util.Deque;
import java.util.LinkedList;

public class MyStack {
	Deque<Integer> firstStack;
	Deque<Integer> secondStack;

	public MyStack() {    
		this.firstStack = new LinkedList();
		this.secondStack = new LinkedList();
	}

	public void push(int x) {
		this.firstStack.add(Integer.valueOf(x));
	}

	public int pop() {
		while (this.firstStack.size() > 1){
			this.secondStack.addLast(this.firstStack.remove()); 
		}
		int lastEl = this.firstStack.remove();
		System.out.println(lastEl);
		this.firstStack.addAll(this.secondStack);
		this.secondStack.clear();
		return lastEl;
	}

	public int top() {
		while (this.firstStack.size() > 1){
			this.secondStack.addLast(this.firstStack.remove());
		}
		int lastEl = this.firstStack.remove();
		System.out.println(lastEl);
		this.secondStack.addLast(lastEl);
		this.firstStack.addAll(this.secondStack);
		this.secondStack.clear();
		return lastEl;
	}


	public boolean empty() {
		return this.firstStack.isEmpty() ? true : false;
	}
}


