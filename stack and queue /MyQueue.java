package queue_stack;

import java.util.Stack;

public class MyQueue {
	Stack<Integer> firstStack = new Stack<>();
	Stack<Integer> secondStack = new Stack<>();  

	public MyQueue() {

	}

	public void push(int x) {
		firstStack.push(x);
	}

	public int pop() {
		while (firstStack.size() > 1){
			secondStack.push(firstStack.pop());
		}
		int removedEl = firstStack.pop().intValue();
		System.out.println(removedEl);
		while(!secondStack.isEmpty()) {
			firstStack.add(secondStack.pop());
		}
		return removedEl;
	}

	public int peek() {
		while (firstStack.size() > 1){
			secondStack.push(firstStack.pop());
		}
		int removedEl = firstStack.pop().intValue();
		secondStack.push(removedEl);
		System.out.println(secondStack);
		while(!secondStack.isEmpty()) {
			firstStack.add(secondStack.pop());
		}
		return removedEl;
	}

	public boolean empty() {
		return firstStack.isEmpty();
	}
}
