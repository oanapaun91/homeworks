package queue_stack;
import java.util.ArrayList;

public class MinStack {
	ArrayList<Integer> myStack = new ArrayList<Integer>();
	int min = Integer.MAX_VALUE;
	ArrayList<Integer> minStack = new ArrayList<Integer>();

	public MinStack() {
	}

	public void push(int val) {
		this.myStack.add(Integer.valueOf(val));
		if (this.top() <= min){
			this.min = this.top();
			minStack.add(this.min);
		}
	}

	public void pop() {
		if (this.top() == this.min && this.minStack.size() -1 > 0){
			minStack.remove(minStack.size()-1);
			this.min = minStack.get(minStack.size()-1);
		} else if (this.top() == this.min && this.minStack.size() - 1 == 0){
			this.min = Integer.MAX_VALUE;
		}
		if (this.myStack.size() != 0){
			this.myStack.remove(myStack.size() - 1);
		}
	}

	public int top() {
		if (this.myStack.size() != 0){
			return this.myStack.get(myStack.size() - 1);
		}
		return 0;
	}

	public int getMin() {
		return this.min;
	}
}
