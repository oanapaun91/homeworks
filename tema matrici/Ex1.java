package matrici;

import java.util.Scanner;
import java.util.ArrayList;

public class Ex1 {

	public static void generareMatriceArray(int matrixDimension, int mainDiagValue, int sndDiagValue, int centerValue, int leftValue, int upValue, int rightValue, int bottomValue){
		int[][] matrice = new int[matrixDimension][matrixDimension];

		for (int i = 0; i < matrixDimension; i++) {
			matrice[i][i] = mainDiagValue;	
			matrice[i][matrixDimension-i-1] = sndDiagValue;
		}

		if (matrixDimension % 2 !=0) {
			matrice[matrixDimension/2][matrixDimension/2] = centerValue;
		}


		for (int i = 0; i < matrixDimension; i++) {
			for (int j = 0; j < matrixDimension; j++){
				
				if (i+j < matrixDimension -1) {
					
					if (i > j) {
						matrice[i][j] = leftValue;
					} else if (i < j){
						matrice[i][j] = upValue;
					}
					
				} else if (i+j > matrixDimension -1){
					
					if (i > j) {
						matrice[i][j] = bottomValue;
					} else if (i < j){
						matrice[i][j] = rightValue;
					}
				} 
			}
		}

		for (int i = 0; i < matrixDimension; i++) {
			for (int j = 0; j < matrixDimension; j++){
				System.out.print(matrice [i][j] + "\t");
			}
			System.out.println();
		}
	}

	public static void generareMatriceArrayList(int matrixDimension, int mainDiagValue, int sndDiagValue, int centerValue, int leftValue, int upValue, int rightValue, int bottomValue) {


		ArrayList<ArrayList<Integer>> matrice = new ArrayList<ArrayList<Integer>>(matrixDimension);

		for (int i = 0; i < matrixDimension; i++) {
			ArrayList<Integer> newRow = new ArrayList<Integer>(matrixDimension);
			for(int j = 0; j <matrixDimension; j++) {
				newRow.add(0);
			}
			matrice.add(newRow);
		}	

		for (int i = 0; i < matrixDimension; i++) {
			for (int j = 0; j < matrixDimension; j++) {
				
				if (i == j) {
					matrice.get(j).set(i,mainDiagValue);
				} else if (j == matrixDimension - i - 1) {
					matrice.get(j).set(i,sndDiagValue);
					
				} else {
					
					if (i+j < matrixDimension -1) {
						if (i > j) {
							matrice.get(i).set(j, leftValue);
						} else {
							matrice.get(i).set(j, upValue);
						}
						
					} else if (i+j > matrixDimension -1){
						
						if (i > j) {
							matrice.get(i).set(j, bottomValue);
						} else {
							matrice.get(i).set(j, rightValue);
						}
					}
				}

			}
		}
		if (matrixDimension % 2 !=0) {
			matrice.get(matrixDimension/2).set(matrixDimension/2, centerValue);
		}

		for(int i = 0; i < matrice.size(); i++) {
			for (int j = 0; j < matrice.get(i).size(); j++) {
				System.out.print(matrice.get(i).get(j) + "\t");
			}
			System.out.println();
		}

	}
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Dimensiunea matricei:");
		int matrixDimension = sc.nextInt();

		System.out.println("Elementele de pe diagonala principala:");
		int mainDiagValue = sc.nextInt();

		System.out.println("Elementele de pe diagonala secundara:");
		int sndDiagValue = sc.nextInt();

		System.out.println("Elementul din centru:");
		int centerValue = sc.nextInt();

		System.out.println("Elementele din stanga:");
		int leftValue = sc.nextInt();

		System.out.println("Elementele din partea de sus:");
		int upValue = sc.nextInt();

		System.out.println("Elementele din dreapta:");
		int rightValue = sc.nextInt();

		System.out.println("Elementele din partea de jos:");
		int bottomValue = sc.nextInt();

		generareMatriceArray(matrixDimension, mainDiagValue, sndDiagValue, centerValue, leftValue, upValue, rightValue,bottomValue);
		System.out.println();
		generareMatriceArrayList(matrixDimension, mainDiagValue, sndDiagValue, centerValue, leftValue, upValue, rightValue,bottomValue);
	}
}

