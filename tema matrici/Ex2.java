package matrici;


public class Ex2 {

	public static void main(String[] args) {


		int[][] matrixA = {{2,10}, {8,1}, {5,5}};
		int[][] matrixB = {{4,3}, {0,3}, {5,10}};

		int m = matrixA.length;
		int n = matrixA[0].length;
		int p = matrixB.length;
		int r = matrixB[0].length;

		if (m == p && n ==r) {
			int[][] adunarea = new int[m][n];
			int[][] scaderea = new int[m][n];
			for (int i = 0; i < m; i++) {
				for (int j = 0; j < n; j++) {
					adunarea[i][j] = matrixA[i][j]+ matrixB[i][j];
					scaderea[i][j] = matrixA[i][j]- matrixB[i][j];
				}
			} 
			
			System.out.println("Adunare");
			for (int i = 0; i < m; i++) {
				for (int j = 0; j < n; j++){
					System.out.print(adunarea[i][j] + "\t");
				}
				System.out.println();
			}

			System.out.println("Scadere");
			for (int i = 0; i < m; i++) {
				for (int j = 0; j < n; j++){
					System.out.print(scaderea[i][j] + "\t");
				}
				System.out.println();
			}

		} else {
			System.out.println("Numarul de linii si coloane trebuie sa fie egale!");
		}


		int scalar = 2;
		int[][] inmultireScalar = new int[m][n];

		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				inmultireScalar [i][j]= scalar * matrixA[i][j];
			}
		}
		System.out.println("Inmultire scalar");
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++){
				System.out.print(inmultireScalar[i][j] + "\t");
			}
			System.out.println();
		}


		int[][] matrixC = {{2,3}, {6,1}, {5,1}};
		int[][] matrixD = {{4,3,0}, {5,5,1}};

		if (matrixC[0].length == matrixD.length) {
			int[][] inmultire = new int[matrixC.length][matrixD[0].length];

			for (int i = 0; i < matrixC.length; i++) {
				for (int j = 0; j < matrixD[0].length; j++) {
					int sum = 0;
					for (int k = 0; k < matrixC[0].length; k++){
						sum += matrixC[i][k] * matrixD[k][j];
					}
					inmultire[i][j] = sum;
				}
			}
			System.out.println("Inmultire");
			for (int i = 0; i < matrixC.length; i++) {
				for (int j = 0; j < matrixD[0].length; j++){
					System.out.print(inmultire[i][j] + "\t");
				}
				System.out.println();
			}
		}
	}

}
