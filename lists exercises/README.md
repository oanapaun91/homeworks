### MergeTwoSortedLists.java
Merge two sorted linked lists and return it as a sorted list. The list should be made by splicing together the nodes of the first two lists.

### MiddleLinkedList.java
Given the head of a singly linked list, return the middle node of the linked list. If there are two middle nodes, return the second middle node.

### OddEvenLinkedList.java 
Given the head of a singly linked list, group all the nodes with odd indices together followed by the nodes with even indices, and return the reordered list. The first node is considered odd, and the second node is even, and so on.

Note that the relative order inside both the even and odd groups should remain as it was in the input.

### RemoveDuplicatesFromSortedList.java 
Given the head of a sorted linked list, delete all duplicates such that each element appears only once. Return the linked list sorted as well.

### RotateList.java
Given the head of a linked list, rotate the list to the right by k places.
